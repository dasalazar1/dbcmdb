// To parse this data:
//
//   import { DatabaseInstance } from "./DatabaseInstance;
//   let value: DatabaseInstance[] = JSON.parse(json);

export interface DatabaseInstance {
    Location:          string;
    Node4:           string;
    InstanceVersion:   string;
    InstanceName:      string;
    IsClustered:       boolean;
    Node2:           string;
    Node1:           string;
    Node3:           string;
    PrimaryRoleDesc:   string;
    Notes:             string;
    PriorityMigration: boolean;
    instanceid:        number;
    OnEva:           boolean;
}

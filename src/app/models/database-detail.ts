// To parse this data:
//
//   import { DatabaseDetails } from "./DatabaseDetails;
//   let value: DatabaseDetails = JSON.parse(json);

export interface DatabaseDetail {
    BusinessContactEmail:  string; //
    MovingTo:              string; //
    AssociatedApp:         string; //
    AssignedTo:            string; //
    BusinessContact:       string; //
    DatabaseName:          string; //
    BusinessContactPhone:  string; //
    ExpectedMigrationDate: string; //
    TechnicalContactEmail: string;
    databaseid:            number; //
    TechincalContact:      string;
    TechnicalContactPhone: string;
    instanceid:            number; //
}

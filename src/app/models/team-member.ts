// To parse this data:
//
//   import { TeamMember } from "./TeamMember;
//   let value: TeamMember[] = JSON.parse(json);

export interface TeamMember {
    MemberName: string;
    id:         number;
}

export interface SQLVersion {
    InstanceVersion: string;
    SPLevel:         string;
    SQLProduct:      string;
    sqlverid:        number;
}

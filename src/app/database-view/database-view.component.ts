import { Component, OnInit, Input } from '@angular/core';
import { DatabaseInstance } from '../models/database-instance';
import { DatabaseDetail } from '../models/database-detail';
import { TeamMember } from '../models/team-member';
import { DatabaseServerService } from '../services/database-server.service';
import { ActivatedRoute } from '@angular/router';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';

@Component({
  selector: 'app-database-view',
  templateUrl: './database-view.component.html',
  styleUrls: ['./database-view.component.css']
})
export class DatabaseViewComponent implements OnInit {

  _details: DatabaseDetail[];
  _instanceid: string;
  _editMode = false;
  _newDatabase: DatabaseDetail;
  _teamMembers: TeamMember[];

  constructor(private dbService: DatabaseServerService, route: ActivatedRoute) {
    route.queryParams.subscribe(params => this._instanceid = params['instanceid']);
   }

  ngOnInit() {
    this.refreshDatabases();
    this.dbService.allTeamMembers().subscribe(data => this._teamMembers = data);
  }

  refreshDatabases() {
    this.dbService.databaseDetail(+this._instanceid).subscribe(data => this._details = data);
    console.log('Refresh Called');
  }

  onDeleted() {
    this.refreshDatabases();
  }

  async onAddDatabase() {
    await this.dbService.insertDatabase(this._newDatabase).subscribe(
      data => {
        console.log(data);
        this.refreshDatabases();
        this._editMode = false;
      }
    );
  }

  onEditAddDatabase() {
    this._editMode = true;
    this._newDatabase = {BusinessContactEmail:  '',
      MovingTo:              '',
      AssociatedApp:         '',
      AssignedTo:            '',
      BusinessContact:       '',
      DatabaseName:          '',
      BusinessContactPhone:  '',
      ExpectedMigrationDate: '',
      TechnicalContactEmail: '',
      TechincalContact:      '',
      TechnicalContactPhone: '',
      instanceid:            +this._instanceid,
      databaseid:            0};
  }
}

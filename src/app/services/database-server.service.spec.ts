import { TestBed, inject } from '@angular/core/testing';

import { DatabaseServerService } from './database-server.service';

describe('DatabaseServerService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DatabaseServerService]
    });
  });

  it('should be created', inject([DatabaseServerService], (service: DatabaseServerService) => {
    expect(service).toBeTruthy();
  }));
});

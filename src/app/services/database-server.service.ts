import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { DatabaseInstance } from '../models/database-instance';
import { DatabaseDetail } from '../models/database-detail';
import { TeamMember } from '../models/team-member';
import { SQLVersion } from '../models/sql-version';
import { Observable } from 'rxjs/Observable';
import { config } from '../app.config';

@Injectable()
export class DatabaseServerService {

  constructor(private http: HttpClient) { }

  public _instances: DatabaseInstance[];

  allInstances(): Observable<DatabaseInstance[]> {
    return this.http.get<DatabaseInstance[]>( config.CMDB_API_URL + 'SQLInstnaces',
    {headers: new HttpHeaders().set('Access-Control-Allow-Origin', '*')});
  }

  allTeamMembers(): Observable<TeamMember[]> {
    return this.http.get<TeamMember[]>( config.CMDB_API_URL + 'TeamMembers',
    {headers: new HttpHeaders().set('Access-Control-Allow-Origin', '*')});
  }

  allSQLVersions(): Observable<SQLVersion[]> {
    return this.http.get<SQLVersion[]>( config.CMDB_API_URL + 'SQLVersions',
    {headers: new HttpHeaders().set('Access-Control-Allow-Origin', '*')});
  }

  databaseDetail(instanceid: number): Observable<DatabaseDetail[]> {
    return this.http.get<DatabaseDetail[]>( config.CMDB_API_URL + 'DatabaseInfoes?instanceid=' + instanceid,
    {headers: new HttpHeaders().set('Access-Control-Allow-Origin', '*')});
  }

  updateInstance(instanceid: number, instance: DatabaseInstance): Observable<any> {
    // params = new HttpParams();
    const sdf  = JSON.stringify(instance);
    return this.http.put(config.CMDB_API_URL + 'SQLInstnaces/' + String(instance.instanceid),
    instance);
    // {headers: new HttpHeaders().set('RequestMethod', 'PUT').set('Content-Type', 'application/json')});
    // {params: new HttpParams().set('instanceid', String(instance.instanceid)) });
    // return Observable.create(0);
  }

  insertInstance(instance: DatabaseInstance): Observable<any> {
    return this.http.post( config.CMDB_API_URL + 'SQLInstnaces/',
    instance);
  }

  deleteInstance(instanceid: number) {
    return this.http.delete( config.CMDB_API_URL + 'SQLInstnaces/' + String(instanceid));
  }

  updateDatabase(databaseid: number, database: DatabaseDetail): Observable<any> {
    return this.http.put( config.CMDB_API_URL + 'DatabaseInfoes/' + String(database.databaseid),
    database);
  }

  insertDatabase(database: DatabaseDetail): Observable<any> {
    return this.http.post( config.CMDB_API_URL + 'DatabaseInfoes/',
    database);
  }

  deleteDatabase(databaseid: number): Observable<any> {
    return this.http.delete( config.CMDB_API_URL + 'DatabaseInfoes/' + String(databaseid));
  }

}

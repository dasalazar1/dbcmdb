import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DatabaseInstance } from '../models/database-instance';
import { DatabaseDetail } from '../models/database-detail';
import { TeamMember } from '../models/team-member';
import { DatabaseServerService } from '../services/database-server.service';
import { ActivatedRoute } from '@angular/router';
import { MatSelectModule } from '@angular/material';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/filter';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[app-database-details]',
  templateUrl: './database-details.component.html',
  styleUrls: ['./database-details.component.css']
})

export class DatabaseDetailsComponent implements OnInit {
  _detail: DatabaseDetail;
  _teamMembers: TeamMember[];
  _editMode = false;
  @Output() isDeleted = new EventEmitter<boolean>();

  ngOnInit() {
    this.dbService.allTeamMembers().subscribe(data => this._teamMembers = data);
    console.log('app details: ' + this._teamMembers);
  }

  @Input() set database(database: DatabaseDetail) {
    this._detail = database;
  }
  constructor(private dbService: DatabaseServerService) { }

  editDatabase() {
    this._editMode = true;
  }

  onCancelEdit() {
    this._editMode = false;
  }

  onUpdateDatabase() {
    console.log('updating');
    // this.http.put('/api/SQLInstnaces/' + instance.instanceid, instance).subscribe(res => { }, (err: HttpErrorResponse) => { });
    this.dbService.updateDatabase(this._detail.databaseid, this._detail).subscribe(data => console.log(data));
    this._editMode = false;
  }

  onDeleteDatabase() {
    console.log('deleting');
    this.dbService.deleteDatabase(this._detail.databaseid).subscribe(data => {
      console.log(data);
      this.isDeleted.emit(true);
    });
  }

}

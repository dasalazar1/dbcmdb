import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatabaseInstanceComponent } from './database-instance.component';

describe('DatabaseInstanceComponent', () => {
  let component: DatabaseInstanceComponent;
  let fixture: ComponentFixture<DatabaseInstanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatabaseInstanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatabaseInstanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

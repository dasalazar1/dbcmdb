import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DatabaseInstance } from '../models/database-instance';
import { DatabaseServerService } from '../services/database-server.service';
import { SQLVersion } from '../models/sql-version';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[app-database-instance]',
  templateUrl: './database-instance.component.html',
  styleUrls: ['./database-instance.component.css']
})
export class DatabaseInstanceComponent {
  _instance: DatabaseInstance;
  _editMode = false;
  _sqlVerions: SQLVersion[];
  @Output() isDeleted = new EventEmitter<boolean>();

  @Input() set instance(instance: DatabaseInstance) {
    this._instance = instance;
  }

  constructor(private dbService: DatabaseServerService) { }

  editInstance() {
    this._editMode = true;
    this.dbService.allSQLVersions().subscribe(data => this._sqlVerions = data);
  }

  instanceDatabases() {
    console.log('Navigate to the deatails page...');
  }

  onCancelEdit() {
    this._editMode = false;
  }

  onUpdateInstance() {
    console.log('updating');
    // this.http.put('/api/SQLInstnaces/' + instance.instanceid, instance).subscribe(res => { }, (err: HttpErrorResponse) => { });
    this.dbService.updateInstance(this._instance.instanceid, this._instance).subscribe(data => console.log(data));
    this._editMode = false;
  }

  onDeleteInstance() {
    console.log('deleting');
    this.dbService.deleteInstance(this._instance.instanceid).subscribe(data => {
      console.log(data);
      this.isDeleted.emit(true);
    });
  }
}


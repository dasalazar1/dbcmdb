import { Component, OnInit } from '@angular/core';
import { DatabaseServerService } from '../services/database-server.service';
import { DatabaseInstance } from '../models/database-instance';
import { SQLVersion } from '../models/sql-version';

@Component({
  selector: 'app-instnace-view',
  templateUrl: './instance-view.component.html',
  styleUrls: ['./instance-view.component.css']
})

export class InstanceViewComponent implements OnInit {
  instances;
  _editMode = false;
  _newInstance: DatabaseInstance;
  _sqlVerions: SQLVersion[];

  constructor(private dbServer: DatabaseServerService) { }

  ngOnInit() {
    this.refreshInsances();
    this.dbServer.allSQLVersions().subscribe(data => this._sqlVerions = data);
  }

  refreshInsances() {
    this.dbServer.allInstances().subscribe(data => this.instances = data);
  }

  onAddInstance() {
    this.dbServer.insertInstance(this._newInstance).subscribe(data => {
      console.log(data);
      this.refreshInsances();
      this._editMode = false;
    });
  }

  onDeleted() {
    this.refreshInsances();
  }

  onEditAddInstance() {
    this._editMode = true;
    this._newInstance = {     Location:          '',
    Node4:           '',
    InstanceVersion:   '',
    InstanceName:      '',
    IsClustered:       false,
    Node2:           '',
    Node1:           '',
    Node3:           '',
    PrimaryRoleDesc:   '',
    Notes:             '',
    PriorityMigration: false,
    instanceid:        0,
    OnEva:            false};
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}

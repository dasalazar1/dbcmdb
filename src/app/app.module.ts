import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { MatSelectModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { InstanceViewComponent } from './instance-view/instance-view.component';
import { DatabaseServerService } from './services/database-server.service';
import { DatabaseInstanceComponent } from './database-instance/database-instance.component';
import { DatabaseDetailsComponent } from './database-details/database-details.component';
import { DatabaseViewComponent } from './database-view/database-view.component';

@NgModule({
  declarations: [
    AppComponent,
    InstanceViewComponent,
    DatabaseInstanceComponent,
    DatabaseDetailsComponent,
    DatabaseViewComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    MatSelectModule,
    HttpClientModule,
    BrowserAnimationsModule,
    RouterModule.forRoot([
      {
        path: 'instances',
        component: InstanceViewComponent
      },
      {
        path: 'databaseDetails',
        component: DatabaseViewComponent
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: '/instances'
      }
    ])
  ],
  providers: [
    DatabaseServerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
